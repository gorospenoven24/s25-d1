// Agregation in mongoDB
// group values from multiple documents together
//  agregation is neede when your application needs form of information that is not visibility available in documents


//  create fruits document
db.fruits.insertMany([
   {
       "name": "Apple",
      "color": "Red",
      "stock": 20,
      "price": 40,
      "supplier_id": 1,
      "onSale": true,
      "origin": ["Philippines", "US"] 
   },
   {
      "name": "Banana",
      "color": "Yellow",
      "stock": 15,
      "price": 20,
      "supplier_id": 2,
      "onSale": true,
      "origin": ["Philippines", "Ecuador"] 
   },
   {
       "name": "Kiwi",
      "color": "Green",
      "stock": 25,
      "price": 50,
      "supplier_id": 1,
      "onSale": true,
      "origin": ["Philippines", "China"] 
   },
   {
       "name": "Mango",
      "color": "Yellow",
      "stock": 10,
      "price": 120,
      "supplier_id": 2,
      "onSale": false,
      "origin": ["Philippines", "India"] 
   }
   ]);

// MongoDB aggregation
// used to generate manipualated data and perform operations to create filtered results that helps in analyzing data


// Using aggregate method
/* $match
	- used to pass documents that meet the specified condition(s) top the next pipeline stage / aggregation process.

	syntax: {$match: {field: value}}


	$group
		- used to gropup elements togther and field-value pairs using the data from the grouped elements

		synatx: {$group :{_id:"value", fieldResult : "valueResult"}}

*/

db.fruits.aggregate([
	{$match :{ "onSale": true }},
	{$group :{ "_id": "$supplier_id", "total" : {$sum: "$stock"} }}
])


// Sorting aggregated results
/* $sort
	- can be used to change the order of aggregated result
	------negative 1 - drecreasing value or reverse output
	_ syntax:{ $sort {field: 1/-1}}
*/

db.fruits.aggregate([
	{$match: { "onSale":true }},
	{$group :{ "_id": "$supplier_id", "total" : {$sum: "$stock"} }},
	{$sort: {"total" : -1}}
]);


//  aggregate results based on array fields
/* $unwind
		-deconstruct an arry field from a collection/ field with array value to output a result for each element

		-syntax: {$unwind: field}

*/
//  with group method
db.fruits.aggregate([
		{$unwind: "$origin"}
		   {$group: {_id: "$origin", kinds: {$sum : 1}}}

	]);

// Schema design 	

// one to one relationship

var owner = ObjectId();

db.owners.insert({
	_id: owner,
	name: "John Smith",
	contact : "09876543210"
});

db.suppliers.insert({
	name: "ABC Fruits",
	contact : "099976543",
	owner_id: ObjectId("617770d7a661137e295f5315")
});



// update the owner document  and add insert the new field
db.owners.updateOne(
	{"_id": ObjectId("617770d7a661137e295f5315")},
		{
			$set : {
				"supplier_id" : ObjectId("61777178a661137e295f5316")
			}
		}
	);


//  One to many relationship
db.suppliers.insertMany([
	{
		name: "DEF Fruits",
		contact : "0963209847",
		owner_id: ObjectId("617770d7a661137e295f5315")
	},
	{
		name: "GJI Fruits",
		contact : "09065489712",
		owner_id: ObjectId("617770d7a661137e295f5315")
	}
	]);


// update the owner document  and add insert the new field
db.owners.updateOne(
	{"_id": ObjectId("617770d7a661137e295f5315")},
		{
			$set : {
				"supplier_id" : [ObjectId("61777178a661137e295f5316"),
				  ObjectId("617779f9a661137e295f5317"), ObjectId("617779f9a661137e295f5318")]
			}
		}
	);

// many to many relationship

var owner = ObjectId();

db.owners.insert({
	_id: owner,
	name: "Jane Doe",
	contact : "09987654321"
});


db.owners.updateOne(
	{"_id": ObjectId("6177810ca661137e295f5319")},
		{
			$set : {
			"supplier_id" : [ObjectId("61777178a661137e295f5316"),
                            ObjectId("61777178a661137e295f5316"), 
                            ObjectId("617779f9a661137e295f5317"), 
                            ObjectId("617779f9a661137e295f5318")]
			}
		}
	);

 // Update the supplier
db.suppliers.updateOne(
	{"_id": ObjectId("617779f9a661137e295f5317")},
		{
			$set : {
			owner_id: [ ObjectId("617770d7a661137e295f5315"), 
        				ObjectId("6177810ca661137e295f5319")]
			}
		}
	);

